#include <stdio.h>
#include <munit.h>

/*
 * Test heh
 *
 * */

static void *
test_heh_setup(const MunitParameter params[], void *user_data)
{
    (void) params;
    (void) user_data;
    int *fixture = malloc(2 * sizeof(int));
    fixture[0] = 1;
    fixture[1] = 2;
    return fixture;
}

static MunitResult
test_heh(const MunitParameter params[], void *fixture)
{
    (void) params;

    munit_assert_int(((int *)fixture)[0], ==, 1);
    munit_assert_int(((int *)fixture)[1], ==, 2);
    return MUNIT_OK;
}

static void
test_heh_teardown(void *fixture)
{
    free(fixture);
}

/*
 * Suite description
 *
 * */

static MunitTest
test_suite_tests[] = {
    {
        (char *) "/heh",
        test_heh,
        test_heh_setup,
        test_heh_teardown,
        MUNIT_TEST_OPTION_NONE,
        NULL
    },
    { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
    (char *) "",
    test_suite_tests,
    NULL,
    1,
    MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char **argv)
{
    return munit_suite_main(&test_suite, NULL, argc, argv);
}
