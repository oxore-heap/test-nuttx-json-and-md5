#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "netutils/cJSON.h"
#include "netutils/md5.h"

int main()
{
    struct cJSON_Hooks hooks =
        (struct cJSON_Hooks){.malloc_fn = malloc, .free_fn = free,};

#define NUM_OF_STRINGS 2
    const char * const strings[NUM_OF_STRINGS] = {
        "[[1,2,772],[2,33,32],[3,2,123]]",
        "[[1,2,776]]",
    };

    cJSON_InitHooks(&hooks);

    for (size_t i = 0; i < NUM_OF_STRINGS; i++) {
        cJSON *json = cJSON_Parse(strings[i]);
        char *json_unformatted = cJSON_PrintUnformatted(json);
        char *hash = md5_hash((unsigned char*)json_unformatted,
                strlen(json_unformatted));

        printf("%s,%s\n", hash, json_unformatted);

        free(json_unformatted);
        free(hash);
    }
}
