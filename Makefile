Q=@
RM=rm

TARGET_MAIN=main
TARGET_TEST=test
MUNIT_OBJECT=deps/munit/munit.c.o

all:

SRC=src
BUILD=build
TARGETS=$(BUILD)/targets

SOURCES+=$(SRC)/cJSON.c
SOURCES+=$(SRC)/md5.c
OBJECTS:=$(patsubst $(SRC)/%,$(BUILD)/%.o,$(SOURCES))

INCLUDE+=./include
INCLUDE+=./deps/munit
INCLUDE:=$(patsubst %,-I%,$(INCLUDE))

#COMMONFLAGS+=-fsanitize=address
#COMMONFLAGS+=--coverage

CFLAGS+=$(INCLUDE)
CFLAGS+=$(COMMONFLAGS)
CFLAGS+=-Wall
CFLAGS+=-Wextra
CFLAGS+=-Wpedantic
CFLAGS+=-Wshadow
CFLAGS+=-Wduplicated-branches
CFLAGS+=-Wduplicated-cond
CFLAGS+=-Wimplicit-fallthrough=0
CFLAGS+=-O3
CFLAGS+=-g3
CFLAGS+=-std=c11

LDFLAGS+=$(COMMONFLAGS)
LDFLAGS+=-Wl,--gc-section
LDFLAGS+=-lm

LDFLAGS_MAIN+=$(LDFLAGS)

LDFLAGS_TEST+=$(LDFLAGS)

all: $(TARGET_MAIN)
ifndef NOTEST
all: $(TARGET_TEST)
endif

$(TARGET_MAIN): $(OBJECTS) $(TARGETS)/$(TARGET_MAIN).c.o
	@ echo "  LD      $@"
	$(Q) $(CC) $(LDFLAGS_MAIN) -o $@ $^

$(TARGET_TEST): $(OBJECTS) $(TARGETS)/$(TARGET_TEST).c.o $(MUNIT_OBJECT)
	@ echo "  LD      $@"
	$(Q) $(CC) $(LDFLAGS_TEST) -o $@ $^

$(MUNIT_OBJECT): $(patsubst %.c.o,%.c,$(MUNIT_OBJECT))
	@ echo "  CC      $@"
	$(Q) $(CC) -c -o $@ $<

$(OBJECTS): | $(BUILD)/
$(OBJECTS): | $(TARGETS)/

%/:
	$(Q) mkdir -p $@

$(BUILD)/%.c.o: $(SRC)/%.c
	@ echo "  CC      $@"
	$(Q) $(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(Q) $(RM) -rfv \
	$(TARGET_MAIN) \
	$(TARGET_TEST) \
	$(OBJECTS) \
	$(MUNIT_OBJECT)

mrproper: clean
	$(Q) $(RM) -rf $(BUILD)

.PHONY: clean mrproper all
